# Programming on an iPad
The goal of this project is to investigate whether it is possible to program using a new iPad. To date, programming is the main task that has kept me going back to my laptop. If I can do relevant web development tasks on an iPad, maybe I can leave the laptop behind... I’d really like to be able to justify getting the larger iPad Pro :-)

A new paragraph.